#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
#include <aquaserver.h>
#include <dimm.h>
#include <controllsled.h>

// Only Dummy
Aquaserver webserver(1);

// Analog
Dimm blue(1);
Dimm marine(2);

//Digital
Controllsled statusled(1,2,3);

int curTime = 0;
int tmpcounter = 0;

File configFile;

void setup() {
	// Open serial communications and wait for port to open:
	Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for Leonardo only
	}
	statusled.setOnNormal();
	initDayTime();

	// start the Ethernet connection and the server:

	// SDCARD open
	// On the Ethernet Shield, CS is pin 4. It's set as an output by default.
	// Note that even if it's not used as the CS pin, the hardware SS pin 
	// (10 on most Arduino boards, 53 on the Mega) must be left as an output 
	// or the SD library functions will not work. 
	pinMode(53, OUTPUT);

	if (!SD.begin(4)) {
		statusled.setOffNormal();
		statusled.setOnError();
		Serial.println("initialization failed!");
		return;
	}

	Serial.println("initialization done.");

}

void loop() {
	getDayTime();
	schedulerTasks();
	delay(1000);
}

void schedulerTasks() {
	//TODO: Auswerten der Daten
	char data = webserver.getAquaserverInput();
	if (curTime == timeToGDT(21, 00)) {
		blue.down(900);
		marine.down(900);
	}
	if (curTime == timeToGDT(9, 00)) {
		blue.rise(900);
		marine.rise(900);
	}
}

/*
 Timestuff
 */

void initDayTime() {
	// Get from the web
	int theTime = 2;
	// TODO: Check if that works
	char server[] = "getthetime.worldhack.de/time.php";
	EthernetClient client;
	client.connect(server, 80);
	if (client.available()) {
		char c = client.read();
		Serial.print(c);
		curTime = c;
	}
	else {
		statusled.setOffNormal();
		statusled.setOnError();
	}

}

int getDayTime() {
	// Minuten die an diese Tag seit 0 Uhr vergangen sind
	tmpcounter++;
	if (tmpcounter == 300) {
		initDayTime();
		tmpcounter = 0;
	}
	if (curTime > 1440) {
		initDayTime();
	}
	return curTime;
}

int timeToGDT(int hours, int minutes) {
	int tmphours = hours * 60;
	return tmphours + minutes;
}