/*
  controllsled.cpp - Library for led checks
  Created by Marc Horst, Juli 26, 2013.
  Released into the public domain.
*/
#include "Arduino.h"
#include "controllsled.h"
#include <Ethernet.h>



Controllsled::Controllsled(int pin_green, int pin_yellow, int pin_red) {
	_pin_1_g = pin_green;
	_pin_2_y = pin_yellow;
	_pin_3_r = pin_red;

	pinMode(_pin_1_g, OUTPUT);
	pinMode(_pin_2_y, OUTPUT);
	pinMode(_pin_3_r, OUTPUT);
}

void Controllsled::setOnWarning() {
	digitalWrite(_pin_2_y, HIGH);
}
void Controllsled::setOnNormal() {
	digitalWrite(_pin_1_g, HIGH);
}
void Controllsled::setOnError() {
	digitalWrite(_pin_3_r, HIGH);
}
void Controllsled::setOffWarning() {
	digitalWrite(_pin_2_y, LOW);
}
void Controllsled::setOffNormal() {
	digitalWrite(_pin_1_g, LOW);
}
void Controllsled::setOffError() {
	digitalWrite(_pin_3_r, LOW);
}
