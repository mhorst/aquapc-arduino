/*
  controllsled.h - Library for led checks
  Created by Marc Horst, Juli 26, 2013.
  Released into the public domain.
*/

#ifndef Controllsled_h
#define Controllsled_h

#include "Arduino.h"

class Controllsled
{
	public:
		Controllsled(int pin_green, int pin_yellow, int pin_red);
		void setOnWarning();
		void setOnNormal();
		void setOnError();
		void setOffWarning();
		void setOffNormal();
		void setOffError();
	private:
		int _pin_1_g;
		int _pin_2_y;
		int _pin_3_r;
};

#endif

