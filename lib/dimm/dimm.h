/*
  Dimm.h - Library for dimming
  Created by Marc Horst, Juli 26, 2013.
  Released into the public domain.
*/

#ifndef Dimm_h
#define Dimm_h

#include "Arduino.h"

class Dimm
{
  public:
    Dimm(int pin);
    void rise(int duration);
    void down(int duration);
    void setOn();
    void setOff();
  private:
    int _pin;
};

#endif
