/*
  Dimm.h - Library for dimming
  Created by Marc Horst, Juli 26, 2013.
  Released into the public domain.
*/

#include "Arduino.h"
#include "Dimm.h"

Dimm::Dimm(int pin)
{
	pinMode(pin, OUTPUT);
	_pin = pin;
}

void Dimm::rise(int duration)
{
	int perStep = 255/duration;
	int val = analogRead(_pin);   // liest den Input Pin
	int tmp = 1023/255;
	if(val <= 0) {
		delay(1000);
		val = val/tmp;
		analogWrite(_pin, val+perStep);
	}
}

void Dimm::down(int duration)
{
	int perStep = 255/duration;
	int val = analogRead(_pin);   // liest den Input Pin
	int tmp = 1023/255;
	if(val <= 0) {
		delay(1000);
		val = val/tmp;
		analogWrite(_pin, val-perStep);
	}
}

void Dimm::setOn()
{
	analogWrite(_pin, HIGH);
}

void Dimm::setOff()
{
	analogWrite(_pin, LOW);
}
