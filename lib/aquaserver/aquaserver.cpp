/*
  Aquaserver.h - Library for dimming
  Created by Marc Horst, Juli 26, 2013.
  Released into the public domain.
*/
#include "Arduino.h"
#include "aquaserver.h"
#include <Ethernet.h>

byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress ip(192,168,1,177);
EthernetServer server(80);
byte sdPin = 4;

#define bufferMax 128
int bufferSize;
char buffer[bufferMax];

Aquaserver::Aquaserver(int pin) {
	Ethernet.begin(mac, ip);
	server.begin();
	Serial.print("server is at ");
	Serial.println(Ethernet.localIP());
}

char Aquaserver::getAquaserverInput() {
	bufferSize = 0;
	EthernetClient client = server.available();
	while (client.connected() && client.available()) {
		char c = client.read();
		if (c == '\n') {
			break;
		}
		else {
			if (bufferSize < bufferMax)
			Serial.print(c);
			Serial.print("\n");
			buffer[bufferSize++] = c;
		}
	}
	return *buffer;
}

void Aquaserver::getTemperatur() {
	//char data = getAquaserverInput();
}
