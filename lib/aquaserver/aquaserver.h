/*
  Aquaserver.h - Library for dimming
  Created by Marc Horst, Juli 26, 2013.
  Released into the public domain.
*/

#ifndef Aquaserver_h
#define Aquaserver_h

#include "Arduino.h"

class Aquaserver
{
	public:
		Aquaserver(int pin);
		char getAquaserverInput();
		void getTemperatur();
	private:
		int _pin;
};

#endif

